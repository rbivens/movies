import React, { Component } from 'react'; 
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/header/Header';
import Home from './components/home/Home';
import Details from './components/details/Details';
import NoMatch from './components/nomatch/NoMatch';
import { CATEGORY_DEFAULT, 
        SESSION_STORAGE_CATEGORY, 
        SESSION_STORAGE_PAGE,
        PATH_HOME,
        PATH_DETAILS_ID } from './components/Constants'

class App extends Component {

  constructor(props) {
    super(props);

    var category;
    if(SESSION_STORAGE_CATEGORY in sessionStorage) {
      category = JSON.parse(sessionStorage.getItem( SESSION_STORAGE_CATEGORY ));
    }
    else {
      category = CATEGORY_DEFAULT;
    }

    var page;
    if(SESSION_STORAGE_PAGE in sessionStorage) {
      page = JSON.parse(sessionStorage.getItem(SESSION_STORAGE_PAGE));
    }
    else {
      page = 1;
    }

    this.state = {
      category: category,
      page: page
    };
  }

  setCategory = (category) => {
    sessionStorage.setItem(SESSION_STORAGE_CATEGORY, JSON.stringify(category));
    this.setState({ category: category, page: 1 });
  }

  setPage = (page) => {
    sessionStorage.setItem(SESSION_STORAGE_PAGE, JSON.stringify(page));
    this.setState({ page: page });
  }

  render() {
    return (
      <div className="App">
          <BrowserRouter>
            <div>
              <Switch>
                <Route exact path={ PATH_HOME } render={() => 
                  <div> 
                    <Header 
                      setCategory={ this.setCategory } 
                      category={ this.state.category } 
                      showCategories={ true }
                      setPage={ this.setPage }  />
                    <Home 
                      category={ this.state.category } 
                      setPage={ this.setPage } 
                      page={ this.state.page } />
                  </div>} />

                <Route exact path={ PATH_DETAILS_ID } render={({ match }) => 
                  <div> 
                      <Header 
                        showCategories={ false } 
                        category={ this.state.category } />
                      <Details 
                        movieId={ match.params.movieId } 
                        page={ this.state.page } />
                    </div>} />
                <Route render={() => <div><NoMatch /></div>} />
              </Switch>
            </div>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
