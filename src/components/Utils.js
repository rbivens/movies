
// Given an array of image widths and a val, 
// returns the image width which is larger than or equal
// the given val. Used when parsing results from 
// themoviedb configuration API.
export function getImageWidth(val, widthArray) {
    var strippedArray = widthArray.filter(item => item.startsWith("w"));
    strippedArray.sort();

    var arrayVal = undefined;
    var i = 0;

    while(strippedArray[i]) {
        arrayVal = strippedArray[i].substring(1);
        if(val <= arrayVal) {
            break;
        }
        i++;
    }
    return "w" + arrayVal;
}
