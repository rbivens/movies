import React from 'react';
import styles from './NoMatch.styl';
import { withStyles } from '@material-ui/core/styles';
import Logo from '../graphics/logo.png';

class NoMatch extends React.Component {
    render() {
        const { classes } = this.props;
        
        return (
            <div className={classes.errorPage}>
                <div>
                    <img src={ Logo } alt='Logo'/>
                </div>
                <br />
                <div >The page could not be found. Recheck your URL or go to <a className={ classes.anchors } href="/">The Ultimate Movie-Goers Guide.</a></div>
            </div>
        );
    }
}

export default withStyles(styles)(NoMatch);