import { COLOR_MOVIE_BLUE } from "../Constants";

const styles = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
    errorPage: { 
      verticalAlign: "middle", 
      color: "white", 
      width: "100%", 
      height: "100%", 
      justifyContent: "center",
      fontSize: "1.1rem",
      paddingTop: 100,
      textAlign: "center",
    },
    anchors: {
      color: COLOR_MOVIE_BLUE
    }
  });

  export default styles;