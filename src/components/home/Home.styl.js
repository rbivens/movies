const styles = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
    container: {
        position: "absolute", 
        top: 64, 
        left: "0px", 
        right: "0px", 
        bottom: "0px", 
        overflowY: "scroll",
        padding: "20px",
        backgroundColor: "black"
    },
    detailsButton: {
      float: "right",
      width: "100%",
      overflow: "hidden",
      textAlign: "right",
      bottom: "10px",
      right: "10px",
      position: "absolute",
    },
    buttonText: {
      color:"#4BA5DA", 
      fontWeight: "700"
    },
    cardFront: {
      backgroundColor: "black", 
      height: "100%",
    },
    cardBack: {
      backgroundColor: "white", 
      color: "#4BA5DA", 
      height: "100%", 
      fontSize: "0.81rem", 
      padding: "16px"
    },
    poster: {
      height: "100%", 
      width: "100%"
    },
    cardBackTitle: {
      fontWeight: "700",
    },
    normalText: {
      color: "black"
    },
    pagination: { 
      width: "100%", 
      display: "flex", 
      justifyContent: "center"
    },
    errorPage: { 
      verticalAlign: "middle", 
      color: "white", 
      width: "100%", 
      height: "100%", 
      display: "flex", 
      justifyContent: "center",
      fontSize: "1.2rem",
      paddingTop: 100,
      textAlign: "center"
    }
  });

  export default styles;