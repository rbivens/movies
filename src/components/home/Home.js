import React from 'react';
import styles from './Home.styl';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { FlippingCard, FlippingCardBack, FlippingCardFront } from 'react-ui-cards';
import axios from 'axios';
import * as Constants from '../Constants';
import { getImageWidth } from "../Utils";
import LoadingScreen from 'react-loading-screen';
import Pagination from 'react-pagination-status';
import "./react-pagination-status.css";
import Poster from '../graphics/missing-poster.png';
import LinesEllipsis from 'react-lines-ellipsis';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';

class Home extends React.Component {

    moviesBeginning = React.createRef();

    constructor(props) {
        super(props);
    
        this.state = {
          movieData: {},
          configData: {},
          isLoading: false,
          error: false,
        };
    }

    // Given a movie category, returns the path
    getMoviePath = (name) => {
        var path = undefined;
        if(name === "Popular") {
            path = "/movie/popular?"
        }
        else if (name === "Now Playing") {
            path = "/movie/now_playing?"
        }
        else {
            path = "/movie/top_rated?";
        }
        return path;
    };

    createMovies = () => {
        const { classes } = this.props;
        var children = [];
        var cnt = 0;
        var resultsArray = this.state.movieData.results;
        
        if(resultsArray) {
            var posterWidth = getImageWidth(Constants.POSTER_CARD_WIDTH, this.state.configData.poster_sizes);
            var baseImageUrl = this.state.configData.secure_base_url;
            var urlPath =  baseImageUrl + posterWidth + "/";

            resultsArray.forEach(function(movie) {
                var posterPath = movie.poster_path;
                var src = (posterPath !== null) ? urlPath + posterPath : Poster;
                var movieId = "/details/" + resultsArray[cnt].id;
                
                children.push(
                    <FlippingCard key={cnt.toString()}>
                        <FlippingCardFront>
                            <div className={ classes.cardFront }>
                                <img src={src} className={ classes.poster } alt='Poster'/>
                            </div>
                        </FlippingCardFront>
                        <FlippingCardBack>
                            <div className={ classes.cardBack }>
                                <div className={ classes.cardBackTitle } >{ movie.title } <br/></div>
                                <div className={ classes.normalText }>
                                    <LinesEllipsis
                                        text={ movie.overview }
                                        maxLine="10"
                                        ellipsis="..."
                                        trimRight
                                        basedOn="letters"
                                        />
                                </div>
                                <br />
                                <div className={ classes.cardBackTitle }>Release Date</div>
                                <div className={ classes.normalText }>
                                    { movie.release_date } 
                                </div>
                                <div className={ classes.detailsButton }>
                                    <div>
                                        <Button size="small" component={ Link } to={ movieId } className={ classes.buttonText }>Details</Button>
                                    </div>
                                </div>
                            </div>
                        </FlippingCardBack>
                    </FlippingCard>);
                cnt++;
            });
        }
        return children;
    }
    
    render() {
        const { classes } = this.props;

        // Bugs occasionally creep up in the endpoints which cause weirdness in total_results.
        // There are many examples of this in the Movies Database Support pages.
        // To fix most of the issues, I make sure that there are no more than 20000 total_results
        // being used (this is in the docs, but often I get back > 20000, which is equivalent
        // to 1000 page returns).
        var totalResults = this.state.movieData.total_results;
        var totalCnt = totalResults > 20000 ? 20000 : totalResults;
        
        return (
            <LoadingScreen
                loading={ this.state.isLoading }
                bgColor={ Constants.COLOR_MOVIE_BLACK }
                spinnerColor={ Constants.COLOR_MOVIE_BLUE }
                textColor={ Constants.COLOR_MOVIE_WHITE }
                text={ Constants.TEXT_LOADING_MOVIES }>

                <div className={ classes.container }>

                { !this.state.error ? (
                    <div ref={ this.moviesBeginning }>
                        <Grid
                            container
                            direction="row"
                            justify="center"
                            alignItems="flex-start">

                            { this.createMovies() }

                        </Grid>
                        <br />
                        <div className={ classes.pagination } >
                            { this.state.movieData.total_results && 
                                <Pagination
                                    handleChangePage = { this.handleChangePage }
                                    activePage = { this.state.movieData.page - 1 }
                                    totalCount = { totalCnt }
                                    perPageItemCount = { 20 }// must hardcode this
                                    nextPageText = { Constants.TEXT_PAGINATION_NEXT }
                                    prePageText = { Constants.TEXT_PAGINATION_PREV }
                                    partialPageCount = { 3 }
                                />
                            }
                        </div>  
                    </div>
                    ):(
                        <div className={ classes.errorPage }>{ Constants.TEXT_ERROR_RETRIEVING_MOVIES }</div>
                    )
                }
                </div>
            </LoadingScreen>
        );
    }

    handleChangePage = (page) => {
        this.props.setPage(page + 1);
        this.fetchData(page + 1);
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.setState({ movieData: {} });
            this.fetchData(this.props.page);
        }
        if(!this.state.error) {
            this.scrollToTop();
        }
      }

    componentDidMount() {
        this.fetchData(this.props.page);
      }

    fetchData = (page) => {
        if(!page) {
            page = 1;
        }
        this.setState({ isLoading: true });
        var configReq = Constants.URL_CONFIURATION + Constants.API_KEY;
        var path = this.getMoviePath(this.props.category)
        var moviesReq = Constants.URL_MOVIE + path + Constants.API_KEY + "&page=" + page;

        // request config info and movies
        axios.get(configReq)
            .then(response =>  {
                    this.setState({ error: false, configData: response.data.images, isLoading: true });

                    axios.get(moviesReq)
                        .then(response =>  this.setState({ error: false, 
                                                        movieData: response.data, 
                                                        isLoading: false }))
                        .catch(error => this.setState({ error: true, isLoading: false }));
                }
            )
            .catch(error => this.setState({ error: true, isLoading: false }));
      }

    scrollToTop = () => {
        this.moviesBeginning.current.scrollIntoView({block: "start", inline: "nearest"})
      }
}

export default withStyles(styles)(Home);
