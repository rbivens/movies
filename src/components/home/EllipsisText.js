import React, { Component, PropTypes } from 'react'
import { ellipsis } from 'react-multiline-ellipsis'
 
class EllipsisText extends Component {
  render () {
    return <div>{this.props.text}</div>
  }
}
 
EllipsisText.propTypes = {
  text: PropTypes.string.isRequired,
}
 
export default ellipsis(EllipsisText, 3, ' (...)');
