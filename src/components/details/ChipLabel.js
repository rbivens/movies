import React from 'react';
import styles from './ChipLabel.styl';
import { withStyles } from '@material-ui/core/styles';

class ChipLabel extends React.Component{

    render() {
        const { classes } = this.props;
        return (
            <div className={ classes.container }>

                <div className={ classes.name } >
                    { this.props.title }
                </div>
                <div className={ classes.value } >
                    { this.props.value }
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(ChipLabel);