const styles = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
    container: {
        float: "left"
    },
    name: {
        backgroundColor: "#555555", 
        fontSize: "13px", 
        color: "white", 
        paddingLeft: 5, 
        paddingRight: 5,
        paddingTop: 2,
        paddingBottom: 2,
        float: "left",
    },
    value: {
        backgroundColor: "#4BA5DA", 
        fontSize: "13px", 
        color: "black", 
        paddingLeft: 5, 
        paddingRight: 5,
        paddingTop: 2,
        paddingBottom: 2,
        float: "left",
        marginRight: "8px"
    },
  });

  export default styles;