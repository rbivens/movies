import * as Constants from "../Constants";

const styles = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
    container: {
        position: "absolute", 
        top: 64, 
        left: "0px", 
        right: "0px", 
        bottom: "0px", 
        padding: "20px",
        backgroundColor: "black",
        overflowY: "auto"
    },
    subContainer: {
      width: "100%"
    },
    errorPage: { 
      verticalAlign: "middle", 
      color: "white", 
      width: "100%", 
      height: "100%", 
      display: "flex", 
      justifyContent: "center",
      fontSize: "1.2rem",
      paddingTop: 100,
      textAlign: "center"
    },
    header: {
      color:"white", 
      fontWeight: 400,
      padding: 6, 
      lineHeight: "1.2rem", 
      verticalAlign: "middle", 
      fontSize: "1rem", 
      width: "80%", 
      background: 'linear-gradient(to right, #333333 , #090909)',
      whiteSpace: "nowrap",
      float: "left", 
      marginRight: "20px"
    },
    gridContainer: {
      width: "95%", 
      margin: "0 auto"
    },
    nameCell: {
      whiteSpace: 'nowrap',
      borderBottom: "0px solid black",
      padding: "8px",
      color: "#eeeeee",
      verticalAlign: "top",
    },
    valueCell: {
      borderBottom: "0px solid black",
      padding: "8px",
      color: "#aaaaaa",
      minWidth: "200px"
    },
    tableRow: {
      height: "14px"
    },
    poster: {
      padding: "10px"
    },
    details: {
      padding: "10px", 
      width: "50%"
    },
    anchors: {
      color: Constants.COLOR_MOVIE_BLUE
    }
  });

  export default styles;