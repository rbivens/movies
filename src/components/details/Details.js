import React from 'react';
import styles from './Details.styl';
import { withStyles } from '@material-ui/core/styles';
import * as Constants from '../Constants';
import axios from 'axios';
import LoadingScreen from 'react-loading-screen';
import Grid from '@material-ui/core/Grid';
import Poster from '../graphics/missing-poster.png';
import { getImageWidth } from "../Utils";
import ChipLabel from "./ChipLabel";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import NumberFormat from 'react-number-format';

var key = 0;
var createData = (name, value) => {
    key += 1;
    return { key, name, value };
}

class Details extends React.Component {
    constructor(props) {
        super(props);
    
        this.state = {
            movieData: {},
            configData: {},
            isLoading: false,
            error: false,
        };
    }

    arrayToString = (array) => {
        var countries;

        for(var i = 0; i < array.length; i++) {
            if(i === 0) {
                countries = "";
            }
            countries += array[i].name;

            if(i !== array.length - 1) {
                countries += ", ";
            }
        }
        return countries;
    }

    createPosterContent = () => {
        const { classes } = this.props;
        var children = [];
        if(this.state.movieData.id) { // <- if movieData.id has loaded, then so has configData
            var posterWidth = getImageWidth(Constants.POSTER_DETAILS_WIDTH, this.state.configData.poster_sizes);
            var baseImageUrl = this.state.configData.secure_base_url;
            var urlPath =  baseImageUrl + posterWidth + "/";
            var posterPath = this.state.movieData.poster_path;
            var src = (posterPath !== null) ? urlPath + posterPath : Poster;
            children.push(<img key="details-0" src={ src } className={ classes.poster } alt='Poster'/>);
        }
        return children;
    }

    createDataContent = () => {
        const { classes } = this.props;

        if(this.state.movieData.id) { 
            var isAdult = this.state.movieData.adult === true ? "Yes" : "No";

            const rows = [
                createData(Constants.TEXT_DATA_TITLE, this.state.movieData.title),
                createData(Constants.TEXT_DATA_TAGLINE, this.state.movieData.tagline),
                createData(Constants.TEXT_DATA_OVERVIEW, this.state.movieData.overview),
                createData(Constants.TEXT_DATA_HOMEPAGE, <a className={ classes.anchors } 
                    href={ this.state.movieData.homepage } >{ this.state.movieData.homepage }</a>),
                createData(Constants.TEXT_DATA_ADULT, isAdult),
                createData(Constants.TEXT_DATA_STATUS, this.state.movieData.status),
                createData(Constants.TEXT_DATA_ORIGINAL_TITLE, this.state.movieData.original_title),
                createData(Constants.TEXT_DATA_POPULARITY, this.state.movieData.popularity),
                createData(Constants.TEXT_DATA_BUDGET, <NumberFormat value={ this.state.movieData.budget } 
                    displayType={ "text" } 
                    thousandSeparator={ true } 
                    prefix={ "$" } /> ),
                createData(Constants.TEXT_DATA_REVENUE, <NumberFormat value={ this.state.movieData.revenue } 
                    displayType={ "text" } 
                    thousandSeparator={ true } 
                    prefix={ "$" } />),
                createData(Constants.TEXT_DATA_VOTE_AVERAGE, this.state.movieData.vote_average),
                createData(Constants.TEXT_DATA_VOTE_COUNT, this.state.movieData.vote_count),
                createData(Constants.TEXT_DATA_GENRES, this.arrayToString(this.state.movieData.genres)),
                createData(Constants.TEXT_DATA_PRODUCTION_COUNTRIES, 
                    this.arrayToString(this.state.movieData.production_countries)),
            ];

            return (
                <Table >
                    <TableBody>
                    {rows.map(row => {
                        return (
                        <TableRow key={ row.key } className={ classes.tableRow }>
                            <TableCell padding="none" className={ classes.nameCell }>{ row.name }</TableCell>
                            <TableCell padding="none" className={ classes.valueCell }>{ row.value }</TableCell>
                        </TableRow>
                        );
                    })}
                    </TableBody>
                </Table>
            )
        }
    }

    render() {
        const { classes } = this.props;
        return (
            <LoadingScreen
                loading={ this.state.isLoading }
                bgColor={ Constants.COLOR_MOVIE_BLACK }
                spinnerColor={ Constants.COLOR_MOVIE_BLUE }
                textColor={ Constants.COLOR_MOVIE_WHITE }
                text={ Constants.TEXT_LOADING_MOVIE_DETAILS }>

                <div className={ classes.container }>
                    { !this.state.error ? (
                        <div className={ classes.subContainer }>
                            <div className={ classes.gridContainer }>
                                <div className={ classes.header } >
                                    <div>{ this.state.movieData.title }</div>
                                </div>
                                <Grid
                                    container
                                    direction="row"
                                    justify="flex-start"
                                    alignItems="flex-start">
                                    <div className={ classes.poster }>
                                        { this.createPosterContent() }
                                    </div>
                                    <div className={ classes.details }>
                                        { this.createDataContent() }
                                    </div>
                                </Grid>
                                <div className={ classes.header } >
                                    <ChipLabel key="details-2" 
                                        title={ Constants.TEXT_CHIPLABEL_RUNTIME } 
                                        value={this.state.movieData.runtime + " minutes"} />
                                    <ChipLabel key="details-3" 
                                        title={ Constants.TEXT_CHIPLABEL_RELEASED } 
                                        value={this.state.movieData.release_date} />
                                </div>
                            </div>
                        </div>
                        ):(
                            <div className={ classes.errorPage }>{ Constants.TEXT_ERROR_RETRIEVING_MOVIES }</div>
                        )
                    }
                </div>
            </LoadingScreen>
        );
    }

    fetchData = () => {
        this.setState({ isLoading: true });
        var configReq = Constants.URL_CONFIURATION + Constants.API_KEY;
        var path = "/movie/";
        var moviesReq = Constants.URL_MOVIE + path + this.props.movieId + "?" + Constants.API_KEY;

        // request config info and movies
        axios.get(configReq)
            .then(response =>  {
                    this.setState({ error: false, configData: response.data.images, isLoading: true });

                    axios.get(moviesReq)
                        .then(response =>  this.setState({ error: false, 
                                                        movieData: response.data, 
                                                        isLoading: false }))
                        .catch(error => this.setState({ error: true, isLoading: false }));
                }
            )
            .catch(error => this.setState({ error: true, isLoading: false }));
    }

    componentDidMount() {
        this.fetchData();
      }
}

export default withStyles(styles)(Details);