import { COLOR_MOVIE_BLUE } from "../Constants";

const styles = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
    select: {
        textAlign: "left",
        fontSize: "0.88rem",
        '&:before': {
            borderColor: COLOR_MOVIE_BLUE,
        },
        '&:after': {
            borderColor: COLOR_MOVIE_BLUE,
        },
        marginRight: "18px",
        width: "240px",
        color: "white",
    },
    '@media (max-width: 600px)': {
        select: {
            width: "120px",
        }
    },
    icon: {
        fill: "white",
    },
    toolbar: {
        position: "absolute",
        width: "100%",
        left: "0px",
        right: "0px",
        top: "0px",
        height: "50px",
        paddingTop: "14px",
        display: "flex",
        overflow: "hidden",
        backgroundColor: "black"
    },
    pushRight: {
        float: "right",
        width: "100%",
        overflow: "hidden"
    },
    menuItem: {
        fontSize: "0.88rem",
    },
    logo: {
        marginLeft: "14px",
    },
  });

  export default styles;