import React from 'react';
import styles from './Header.styl';
import { withStyles } from '@material-ui/core/styles';
import { CATEGORY_ALL } from '../Constants'
import Logo from '../graphics/logo.png';
import SmallLogo from '../graphics/logo-sm.png';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const mql = window.matchMedia(`(min-width: 600px)`);

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.mediaQueryChanged = this.mediaQueryChanged.bind(this);

        this.state = {
          searchKey: this.props.category,
          renderLargeLogo: mql.matches 
        };
      }

    render() {
        const { classes } = this.props;
        return (
            <div className={ classes.toolbar }>
                <div className={ classes.logo }>
                    { this.state.renderLargeLogo ? (
                        <img src={ Logo } alt='Logo'/>
                    ):(
                        <img src={ SmallLogo } alt='Logo'/>
                    )
                    }
                </div>
                <div className={ classes.pushRight } />
                { this.props.showCategories &&
                    <div>
                        <Select
                            className={ classes.select }
                            onChange={(e) => {
                                var val = e.target.value;
                                this.setState({ searchKey: val });
                                this.props.setCategory(val);
                                this.props.setPage(1);
                            }}
                            inputProps={{ classes: { icon: classes.icon } }}
                            value={ this.state.searchKey }
                        >
                        {CATEGORY_ALL.map(name => (
                            <MenuItem
                                key={ name }
                                value={ name }
                                className={ classes.menuItem }
                            >
                                { name }
                            </MenuItem>
                            ))}
                        </Select>
                    </div>
                }
            </div>
        );
      }

    componentWillMount() {
        mql.addListener(this.mediaQueryChanged);
    }
    
    componentWillUnmount() {
        mql.removeListener(this.mediaQueryChanged);
    }

    mediaQueryChanged() {
        this.setState({ renderLargeLogo: mql.matches });
      }
}

export default withStyles(styles)(Header);