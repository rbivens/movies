// predominant colors for movie app
export const COLOR_MOVIE_BLUE = "#4BA5DA";
export const COLOR_MOVIE_BLACK = "#000000";
export const COLOR_MOVIE_WHITE = "#FFFFFF";

// movie categories (referenced in Header.js)
export const CATEGORY_ALL = [ 'Now Playing', 'Popular', 'Top Rated' ];
export const CATEGORY_DEFAULT = "Popular";

// urls and related
export const API_KEY = "api_key=58328413417b76bbb03c4401f9ac324a";
export const URL_CONFIURATION = "https://api.themoviedb.org/3/configuration?";
export const URL_MOVIE = "https://api.themoviedb.org/3";

// requested poster width
export const POSTER_CARD_WIDTH = 300;
export const POSTER_DETAILS_WIDTH = 300;

// session storage for page refreshes
export const SESSION_STORAGE_CATEGORY = "umgg:currentCategory";
export const SESSION_STORAGE_PAGE = "umgg:currentPage";

// url paths
export const PATH_HOME = "/";
export const PATH_DETAILS = "/details/";
export const PATH_DETAILS_ID = "/details/:movieId";

// application text
export const TEXT_LOADING_MOVIES = "Loading Movies...";
export const TEXT_LOADING_MOVIE_DETAILS = "Loading Movie Details...";
export const TEXT_PAGINATION_NEXT = ">";
export const TEXT_PAGINATION_PREV = "<";
export const TEXT_ERROR_RETRIEVING_MOVIES = "There was a problem retrieving the movie list";

export const TEXT_DATA_TITLE = "Title";
export const TEXT_DATA_TAGLINE = "Tagline";
export const TEXT_DATA_OVERVIEW = "Overview";
export const TEXT_DATA_HOMEPAGE = "Homepage";
export const TEXT_DATA_ADULT = "Adult";
export const TEXT_DATA_STATUS = "Status";
export const TEXT_DATA_ORIGINAL_TITLE = "Original Title";
export const TEXT_DATA_POPULARITY = "Popularity";
export const TEXT_DATA_BUDGET = "Budget";
export const TEXT_DATA_REVENUE = "Revenue";
export const TEXT_DATA_VOTE_AVERAGE = "Vote Average";
export const TEXT_DATA_VOTE_COUNT = "Vote Count";
export const TEXT_DATA_GENRES = "Genres";
export const TEXT_DATA_PRODUCTION_COUNTRIES = "Production Countries";

export const TEXT_CHIPLABEL_RUNTIME = "run time";
export const TEXT_CHIPLABEL_RELEASED = "released";




